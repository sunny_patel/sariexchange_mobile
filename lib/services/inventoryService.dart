import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:async/async.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:sariexchange_mobile/helpers.dart';
import 'package:sariexchange_mobile/models/inventory.dart';
import 'package:sariexchange_mobile/models/inventoryItem.dart';
import 'package:sariexchange_mobile/models/user.dart';
import 'package:sariexchange_mobile/services/appFirebaseService.dart';
import 'package:sariexchange_mobile/services/usersService.dart';

class InventoryService extends AppFirebaseService {
  static InventoryService _inventoryService;
  final String host = 'https://24cf8517-d1ca-4125-ad15-fb46035141cc.mock.pstmn.io';
  final String endpoint = '/inventory';
  DatabaseReference invRef;
  UsersService usersService;
  String url;
  
  factory InventoryService() {
    if (_inventoryService == null) {
      _inventoryService = new InventoryService._create(); // Causes singleton instantiation 
    }
    return _inventoryService;
  }

  InventoryService._create() {
    this.url = host + endpoint;
    invRef = this.dbRef.child('inventory');
    usersService = UsersService();
  }

  Future<http.Response> fetchPost() {
    return http.get(this.url);
  }

  Future<Inventory> fetchInventory() async {
    final response =
        await http.get(this.url);

    if (response.statusCode == 200) {
      print('Received inventory');
      print(json.decode(response.body));
      // If server returns an OK response, parse the JSON
      return Inventory.fromJson(json.decode(response.body));
    } else {
      print('Error getting inventory');
      // If that response was not OK, throw an error.
      throw Exception('Failed to load post');
    }
  }

  Stream<InventoryItem> inventorysStream() async* {
    await for(Event event in this.invRef.onChildAdded) {
      print('inventoryService');
      
      yield InventoryItem.fromDataSnapshot(event.snapshot);
    }
  }

  Future<bool> saveItem(InventoryItem item) async {
    FirebaseUser user = await this.usersService.getUser();
    if (item.images.length == 0) {
      item.addImage(Helpers.getSampleImage(width: 600, height: 400, text: "Image Placeholder"));
    }
    var itemJson = item.toJson();
    print('Adding item:');
    print(itemJson);
    try {
      var fbTasks = List<Future>();

      fbTasks.add(this.usersService.userRef
        .child(user.uid + '/inventory/').child(item.id)
        .set(itemJson));
      
      fbTasks.add(this.invRef
        .child(item.id).set(itemJson));

      await Future.wait(fbTasks);
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  Stream<double> uploadImageForInventoryItem(InventoryItem item, File imageFile, [onComplete(StorageUploadTask uploadTask)]) async* {
    var imgPath = item.id + '/' + Helpers.uuidV1() + '.jpg';
    final StorageReference firebaseStorageRef =
          FirebaseStorage.instance.ref().child(imgPath);
    final StorageUploadTask imageUploadTask =
        firebaseStorageRef.putFile(imageFile);

    await for(var onData in imageUploadTask.events) {
      switch (onData.type) {
        case StorageTaskEventType.progress: 
          var progress = onData.snapshot.bytesTransferred;
          var total = onData.snapshot.totalByteCount;
          var percentageCompleted = progress / total;
          yield percentageCompleted;
        break;
        case StorageTaskEventType.success:
          HapticFeedback.heavyImpact();
          onComplete(imageUploadTask);
          yield 0;
        break;
        default:
        break;
      }
    }
  }
}
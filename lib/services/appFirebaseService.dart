
import 'package:firebase_database/firebase_database.dart';

abstract class AppFirebaseService {
  final dbRef = FirebaseDatabase.instance.reference();
}
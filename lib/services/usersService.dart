import 'dart:io';
import 'package:firebase_database/firebase_database.dart';
import 'package:sariexchange_mobile/services/appFirebaseService.dart';
import 'package:firebase_auth/firebase_auth.dart';

class UsersService extends AppFirebaseService{
  static UsersService _userService;
  static final FirebaseAuth _auth = FirebaseAuth.instance;
  DatabaseReference userRef;

  factory UsersService() {
    if (_userService == null) {
      _userService = new UsersService._create(); // Causes singleton instantiation 
    }
    return _userService;
  }

  UsersService._create() {
    userRef = this.dbRef.child('users');
  }

  init() async {
    await this._handleSignin();
  }

  Future<FirebaseUser> getUser() async {
    var currentUser = await _auth.currentUser();
    if (currentUser == null) {
      print('Creating new user');
      return await _handleSignin();
    } else {
      print('Returning user');
      return currentUser;
    }
  }

  Future<FirebaseUser> _handleSignin() async {
    return await _auth.signInAnonymously();
  }
}
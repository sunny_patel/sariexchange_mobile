import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:sariexchange_mobile/helpers.dart';
import 'package:sariexchange_mobile/services/inventoryService.dart';
import 'package:sariexchange_mobile/services/usersService.dart';
import 'package:sariexchange_mobile/models/inventoryItem.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:sariexchange_mobile/shared/appTextField.dart';

class NewItemModal extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return _NewItemModalState();
  }
}

class _NewItemModalState extends State<NewItemModal> {
  InventoryItem invItem;
  double uploadProgress;
  UsersService usersService;
  InventoryService inventoryService;

  @override
  void initState() {
    super.initState();
    this.inventoryService = InventoryService();
    this.usersService = UsersService();
    this.invItem = InventoryItem();
    this.uploadProgress = 0.0;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Container(
          child: ListView(
            children: <Widget>[
              Container(
                child: ConstrainedBox(
                  constraints: BoxConstraints.loose(
                    Size(
                      Helpers.getWidth(context),
                      Helpers.getThirdOfHeight(context)
                    )
                  ),
                  child: _buildGallery(context),
                )
              ),
              this._showLinearProgressIndicator(),
              this._buildForm()
            ],
          )
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add_a_photo),
        onPressed: openCamera,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 10.0),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              FlatButton(
                padding: EdgeInsets.symmetric(
                    vertical: 10.0,
                    horizontal: 25.0
                ),
                onPressed: () => this._save(),
                child: Text('Save'),
                textTheme: ButtonTextTheme.primary,
              ),
              Spacer(),
              FlatButton(
                padding: EdgeInsets.symmetric(
                    vertical: 10.0,
                    horizontal: 25.0
                ),
                onPressed: () => this.close(),
                child: Text('Cancel'),
                textTheme: ButtonTextTheme.normal,
              ),
            ],
          )
        )
      ),
    );
  }

  onEditingTitleComplete(String title) {
    this.invItem.title = title;
  }

  onEditingDescriptionComplete(String desc) {
    this.invItem.subTitle = desc;
  }

  onEditingPriceComplete(String price) {
    this.invItem.price = price;
  }

  _showLinearProgressIndicator() {
    if (this.uploadProgress > 0) {
      return LinearProgressIndicator();
    }
    return Container();
  }

  _buildForm() {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: 5.0,
        horizontal: 20.0
      ),
      child: Column(
        children: <Widget>[
          AppTextField(
            hintText: 'Title',
            onChanged: (v) => onEditingTitleComplete(v),
          ),
          AppTextField(
            hintText: 'Description',
            onChanged: (v) => onEditingDescriptionComplete(v),
          ),
          AppTextField(
            hintText: 'Price',
            onChanged: (v) => onEditingPriceComplete(v),
            keyboardType: TextInputType.numberWithOptions(
              decimal: true
            ),
            prefixIcon: Icon(Icons.attach_money),
          ),
        ],
      )
    );
  }

  _buildGallery(BuildContext context) {
    var width = Helpers.getWidth(context);
    var height = Helpers.getThirdOfHeight(context);

    if (this.invItem.images != null && this.invItem.images.length > 0) {
      return this._buildSwiper();
      // return ListView.builder(
      //   scrollDirection: Axis.horizontal,
      //   itemCount: this.invItem.images.length,
      //   itemBuilder: (context, index) {
      //     return this._buildImageWithFullScreen(this.invItem.images.elementAt(index), width, height);
      //   },
      // );
    } else {
      return Column(
        children: <Widget>[
          GestureDetector(
            onTap: openCamera,
            child: Image(
              image: CachedNetworkImageProvider(Helpers.getSampleImage(
                width: width,
                height: height,
                text: 'Tap to add an image'
              )),
              fit: BoxFit.fill
            )
          )
        ],
      );
    }
  }

  Swiper _buildSwiper() {
    return Swiper(
      itemBuilder: (BuildContext context, int index) {
        return new Image(
          image: CachedNetworkImageProvider(this.invItem.images.elementAt(index)),
          fit: BoxFit.fitWidth,
        );
      },
      itemCount: this.invItem.images.length,
      pagination: SwiperPagination(),
      viewportFraction: .8,
      scale: .9,
    );
  }

  _buildImageWithFullScreen(String imageUri, double w, double h) {
    return GestureDetector(
      onTap: () {},
      onLongPress: () {
        // open bottom sheet for delete, set as primary
      },
      child: Image(
        image: CachedNetworkImageProvider(imageUri),
        width: w,
        height: h,
      )
    );
  }
  
  openCamera() async {
    File imageFile = await ImagePicker.pickImage(source: ImageSource.camera);
    this.inventoryService
      .uploadImageForInventoryItem(this.invItem, imageFile, this._onImageUploadComplete)
      .listen((percentageCompleted) {
        setState(() {
          this.uploadProgress = percentageCompleted;
        });
      });
  }

  _onImageUploadComplete(StorageUploadTask task) async {
    var completedUloaded = await task.onComplete;
    var downloadUri = await completedUloaded.ref.getDownloadURL();
    print(downloadUri);

    setState(() {
      this.invItem.addImage(downloadUri);
    });
  }

  close() async {
    Navigator.pop(context, this.invItem);
  }

  _save() async {
    if (this.invItem.isValid()) {
      var saved = await this.inventoryService.saveItem(this.invItem);
      if (saved) {
        this.close();
      } else {
        print('Error saving inv item');
      }
    }
  }

}
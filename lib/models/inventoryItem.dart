import 'package:firebase_database/firebase_database.dart';
import 'package:sariexchange_mobile/helpers.dart';

class InventoryItem {
  String id;
  String title;
  String subTitle;
  String price;
  List<String> images;

  InventoryItem({
    this.id,
    this.title,
    this.subTitle,
    this.price,
    this.images
  }) {
    this.id = this.id == null ? Helpers.uuidV1() : this.id;
    this.images = this.images == null ? List<String>() : this.images;
  }

  factory InventoryItem.fromJson(Map<String, dynamic> json) {
    return InventoryItem(
      id: json['id'],
      title: json['title'],
      subTitle: json['subtitle'],
      price: json['price'] != null ? json['price'] : '0',
      images: json['images'],
    );
  }

  factory InventoryItem.fromDataSnapshot(DataSnapshot snapshot){
    var inventoryMap = snapshot.value as Map;
      
    return InventoryItem(
      id: inventoryMap['id'],
      title: inventoryMap['title'],
      subTitle: inventoryMap['subTitle'],
      price: inventoryMap['price'],
      images: List<String>.from(inventoryMap['images'])
    );
  }

  Map<String, dynamic> toJson() => {
    'id': this.id,
    'title': this.title,
    'subTitle': this.subTitle,
    'price': this.price,
    'images': this.images
  };

  addImage(String imgUri) {
    if (this.images == null) {
      this.images = List<String>();
    }
    this.images.add(imgUri);
  }

  bool isValid() {
    if (
      this.title == null ||
      this.subTitle == null ||
      this.price == null
    ) {
      return false;
    }
    return true;
  }
}
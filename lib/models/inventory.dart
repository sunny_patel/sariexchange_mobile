import 'inventoryItem.dart';

class Inventory {
  List<InventoryItem> items;
  
  Inventory({this.items});

  factory Inventory.fromJson(Map<String, dynamic> json) {
    return Inventory(items: json['inventory'].map<InventoryItem>((inventoryItem) {
      return InventoryItem(
        id: inventoryItem['id'],
        title: inventoryItem['title'],
        subTitle: inventoryItem['subtitle'],
        images: [inventoryItem['imageUrl']]
      );
    }).toList());
  }
}
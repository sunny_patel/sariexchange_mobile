import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AppTextField extends StatelessWidget {
  final String hintText;
  final ValueChanged<String> onChanged;
  final TextInputType keyboardType;
  final List<TextInputFormatter> inputFormatters;
  final Icon prefixIcon;

  AppTextField({
    this.hintText,
    this.onChanged,
    this.keyboardType,
    this.inputFormatters,
    this.prefixIcon
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
        vertical: 5.0
      ),
      padding: EdgeInsets.symmetric(
        vertical: 5.0
      ),
      child: TextField(
        autocorrect: false,
        inputFormatters: this.inputFormatters != null ? this.inputFormatters : [],
        keyboardType: this.keyboardType != null ? this.keyboardType : TextInputType.text,
        style: TextStyle(
          color: Colors.black,
          fontSize: 18.0
        ),
        decoration: InputDecoration(
          prefixIcon: this.prefixIcon != null ? this.prefixIcon : null,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5.0)
          ),
          labelText: this.hintText,
          filled: true,
        ),
        onChanged: this.onChanged,
      )
    );
  }
  
}
import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:sariexchange_mobile/helpers.dart';
import 'package:sariexchange_mobile/models/inventoryItem.dart';
import 'package:sariexchange_mobile/shared/inventoryTiles/inventoryTile.dart';

class InventoryTilesSwiper extends StatefulWidget {
  final Stream<InventoryItem> inventoryItemsStream;
  final String title;

  InventoryTilesSwiper({this.inventoryItemsStream, this.title});

  @override
  _InventoryTilesSwiperState createState() => _InventoryTilesSwiperState();
}

class _InventoryTilesSwiperState extends State<InventoryTilesSwiper> {
  List<InventoryItem> invItems;
  StreamSubscription<InventoryItem> invItemStreamSubscription;

  @override
  void initState() {
    print('initState');
    super.initState();
    this.invItems = List<InventoryItem>();
    this.invItemStreamSubscription = widget.inventoryItemsStream.listen((InventoryItem newInvItem) {
      this._addInventoryItem(newInvItem);
    });
  }

  @override
  void dispose() {
    print('disposing');
    this.invItemStreamSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: this._buildSlider(context)
    );
  }

  Swiper _buildSwiper(BuildContext context) {
     return Swiper(
      itemBuilder: (BuildContext context, int index) {
        return new Image(
          image: CachedNetworkImageProvider(this.invItems.elementAt(index).images[0]),
          fit: BoxFit.fill,
        );
      },
      itemCount: 10,
      viewportFraction: 0.8,
      scale: 0.9,
    );
  }

  Widget _buildSlider(BuildContext context) {
    var sliderWidth = Helpers.getWidth(context);
    var sliderHeight = Helpers.getThirdOfHeight(context);
    var title = widget.title != null ? widget.title : '';

    return Container(
        margin: EdgeInsets.symmetric(
          vertical: 10
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: 15.0,
                        vertical: 10.0
                    ),
                    child: Text(
                      title,
                      style: Theme.of(context).textTheme.display3,
                      textAlign: TextAlign.left,
                    )
                )
              ],
            ),
            ConstrainedBox(
                constraints: BoxConstraints.loose(Size(sliderWidth, sliderHeight)),
                child: this.buildTilesWidget()
            )
          ],
        )
    );
  }

  Widget buildTilesWidget() {
    if (invItems != null && invItems.length > 0) {
      return Swiper(
        itemBuilder: (BuildContext context, int index) {
          return Container(
            margin: EdgeInsets.all(10.0),
            child: InventoryTile(invItem: invItems.elementAt(index))
          );
        },
        itemCount: invItems.length,
        viewportFraction: 0.8,
        scale: 1.0,
        loop: false,
        layout: SwiperLayout.DEFAULT,
      );
    } else {
      return Text('No items at this time.');
    }
  }

  _addInventoryItem(InventoryItem invItem) {
    setState(() {
      invItems.add(invItem);
    });
  }
}
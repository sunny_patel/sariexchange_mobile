import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:sariexchange_mobile/helpers.dart';
import 'package:sariexchange_mobile/models/inventoryItem.dart';
import 'package:sariexchange_mobile/screens/itemDetails.dart';

class InventoryTile extends StatelessWidget {
  final InventoryItem invItem;

  InventoryTile({this.invItem});

  Widget buildTile(BuildContext context) {

    return GestureDetector(
      onTap: () {
        Navigator.push(context, MaterialPageRoute(
          fullscreenDialog: true,
          builder: (context) => ItemDetails(currentItem: this.invItem)
        ));
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _buildImage(context),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(this.invItem.title, style: Theme.of(context).textTheme.title),
                    Text(this.invItem.price, style: Theme.of(context).textTheme.body2)
                  ],
                ),
                Text(this.invItem.subTitle, style: Theme.of(context).textTheme.subtitle)
              ],
            )
          )

        ],
      )
    );
  }

  @override
  Widget build(BuildContext context) {
    return this.buildTile(context);
  }

  Container _buildImage(BuildContext context) {
    if (this.invItem == null || this.invItem.images == null || this.invItem.images.length == 0) {
      return Container();
    }

    return Container(
      child: ClipRRect(
        borderRadius: new BorderRadius.circular(8.0),
        child: Image(
          image: CachedNetworkImageProvider(this.invItem.images[0]),
          fit: BoxFit.fitWidth,
          height: Helpers.getThirdOfHeight(context),
          width: Helpers.getWidth(context)
        )
      )
    );
  }
  
}
import 'package:flutter/material.dart';

// Color from: https://material.io/design/components/bottom-navigation.html#theming
final MaterialColor appColorSwatch = new MaterialColor(0xFF0336FF, {
  50: Color(0xFF0336FF),
  100: Color(0xFF0336FF),
  200: Color(0xFF355eff),
  300: Color(0xFF0336FF),
  400: Color(0xFF0336FF),
  500: Color(0xFF0336FF),
  600: Color(0xFF0336FF),
  700: Color(0xFF0225b2),
  800: Color(0xFF0336FF),
  900: Color(0xFF0025b6),
});

final Color appPrimaryColor = Color(0xFF0336FF);
final Color appDangerColor = Color(0xFFB00020);
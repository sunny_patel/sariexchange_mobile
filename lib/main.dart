import 'package:flutter/material.dart';
import 'package:sariexchange_mobile/screens/boutique.dart';
import 'package:sariexchange_mobile/screens/ourPicks.dart';
import 'package:sariexchange_mobile/services/usersService.dart';
import 'package:sariexchange_mobile/theme.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    this.init();
    return MaterialApp(
      title: 'Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or press Run > Flutter Hot Reload in IntelliJ). Notice that the
        // counter didn't reset back to zero; the application is not restarted.
        primarySwatch: appColorSwatch,
        buttonTheme: ButtonThemeData(
          textTheme: ButtonTextTheme.primary
        ),
        textTheme: TextTheme(
          title: TextStyle(
            fontWeight: FontWeight.w500,
            fontFamily: 'Raleway',
            fontSize: 18
          ),
          subtitle: TextStyle(
              fontWeight: FontWeight.w500,
              fontFamily: 'Raleway',
              fontSize: 14
          ),
          display3: TextStyle(
            fontSize: 24.0,
            color: Colors.black,
            fontFamily: 'Raleway',
            fontWeight: FontWeight.w700
          ),
          button: TextStyle(
            fontSize: 16.0,
            fontFamily: 'Raleway',
            fontWeight: FontWeight.w600
          )
        )
      ),
      home: DefaultTabController(
        length: 4,
        child: Scaffold(
          bottomNavigationBar: TabBar(
            indicatorSize: TabBarIndicatorSize.label,
            indicatorColor: Theme.of(context).bottomAppBarColor,
            labelColor: appPrimaryColor,
            unselectedLabelColor: Colors.black12,
            labelPadding: EdgeInsets.only(bottom: 10),
            tabs: [
              Tab(icon: Icon(Icons.home)),
              Tab(icon: Icon(Icons.search)),
              Tab(icon: Icon(Icons.business)),
              Tab(icon: Icon(Icons.person))
            ],
          ),
          body: TabBarView(
            children: [
              OurPicks(),
              Icon(Icons.search),
              Boutique(),
              Icon(Icons.person)
            ]
          )
        ),
      )
    );
  }

  init() async {
    var userService = UsersService();
    userService.init();
  }
}
import 'package:flutter/material.dart';
import 'package:sariexchange_mobile/models/inventoryItem.dart';
import 'package:sariexchange_mobile/models/NewItemModal.dart';

class Boutique extends StatefulWidget {
  @override
  BoutiqueState createState() => BoutiqueState();
}

class BoutiqueState extends State<Boutique> with AutomaticKeepAliveClientMixin {
  List<InventoryItem> inventoryItems = List<InventoryItem>();

  Widget build(BuildContext context) {
    super.build(context);
    
    return Scaffold(
      body: ListView(
        children: <Widget>[
          
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => _navNewItemModal(context),
      ),
    );
  }

  void _navNewItemModal(BuildContext context) async {
    InventoryItem item = await Navigator.push(
      context,
      MaterialPageRoute(
        fullscreenDialog: true,
        builder: (context) => NewItemModal()
      )
    );
    if (item != null) {
      setState(() {
        // inventoryItems.add(item);
      });
    }
  }

  @override
  bool get wantKeepAlive => true;
}


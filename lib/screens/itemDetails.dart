import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:sariexchange_mobile/screens/fullscreenView.dart';
import 'package:sariexchange_mobile/models/inventoryItem.dart';
import 'package:sariexchange_mobile/theme.dart';

import '../helpers.dart';

class ItemDetails extends StatefulWidget {
  final InventoryItem currentItem;

  ItemDetails({this.currentItem});

  @override
  State<StatefulWidget> createState() {
    return _ItemDetailsState();
  }
}

class _ItemDetailsState extends State<ItemDetails> {
  String title;
  String description;
  String price;
  List<String> images;

  @override
  void initState() {
    super.initState();
    this.title = widget.currentItem.title;
    this.description = widget.currentItem.subTitle;
    this.price = widget.currentItem.price != null ? widget.currentItem.price : 0;
    this.images = List<String>();
    if (widget.currentItem.images.length > 0) {
      widget.currentItem.images.forEach((image) {
        this.images.add(image);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(this.title),
        centerTitle: false,
        elevation: 0.0,
        iconTheme: IconThemeData(
          color: Theme.of(context).primaryColor
        ),
        textTheme: Theme.of(context).textTheme,
        backgroundColor: Colors.transparent,
        brightness: Brightness.light,
      ),
      body: Container(
        child: ListView(
          children: <Widget>[
            Container(
              child: ConstrainedBox(
                constraints: BoxConstraints.loose(
                  Size(
                    Helpers.getWidth(context),
                    Helpers.getThirdOfHeight(context)
                  )
                ),
                child: _buildGallery(),
              )
            ),
            Container(
              padding: EdgeInsets.all(10.0),
              child: Row(
                children: <Widget>[
                  Text(this.description, style: Theme.of(context).textTheme.body1),
                  Text(this.price)
                ],
              )
            )
          ],
        )
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: Icon(Icons.edit),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat
    );
  }

  _buildGallery() {
    var width = Helpers.getWidth(context);
    var height = Helpers.getThirdOfHeight(context);

    print('width' + width.toString());
    print('height' + height.toString());

    if (this.images.length > 0) {
      return this._buildSwiper(this.images);
    } else {
      return Column(
        children: <Widget>[
          Image(image: CachedNetworkImageProvider('https://via.placeholder.com/400x250?text=Add+Images'))
        ],
      );
    }
  }

  Widget _buildSwiper(List<String> images) {
    return Container(
      child: Swiper(
        itemBuilder: (BuildContext context, int index) {
          return Card(
            margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
            clipBehavior: Clip.antiAlias,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
            elevation: 4.0,
            child: GestureDetector(
              onTap: () {
                Navigator.push(context, MaterialPageRoute(
                  fullscreenDialog: true,
                  builder: (context) => FullscreenView(
                    title: this.title,
                    imageUrl: images.elementAt(index)
                  )
                ));
              },
              child: Image(
                image: CachedNetworkImageProvider(images.elementAt(index)),
                fit: BoxFit.fitWidth,
              )
            )
          );
        },
        itemCount: images.length,
        viewportFraction: 0.9,
        scale: 1,
        loop: false,
        pagination: RectSwiperPaginationBuilder(),
      )
    );
  }

  close() async {
    Navigator.pop(context, null);
  }
}
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:photo_view/photo_view.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:sariexchange_mobile/theme.dart';

class FullscreenView extends StatefulWidget {
  final String title;
  final String imageUrl;

  FullscreenView({this.title, this.imageUrl});

  @override
  State<StatefulWidget> createState() {
    return _FullscreenView();
  }
}
//Navigator.pop(context, null);
class _FullscreenView extends State<FullscreenView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        centerTitle: false,
        backgroundColor: Colors.white,
        elevation: 0.0,
        brightness: Brightness.light,
        iconTheme: IconThemeData(color: appPrimaryColor),
        textTheme: Theme.of(context).textTheme,
      ),
      body: this._buildPhotoView(),
    );
  }

  _buildPhotoView() {
    return Container(
      color: Colors.black,
      child: PhotoView(
        imageProvider: CachedNetworkImageProvider(widget.imageUrl),
        backgroundDecoration: BoxDecoration(color: Colors.white),
      )
    );
  }
}



import 'dart:async';

import 'package:flutter/material.dart';
import 'package:sariexchange_mobile/models/inventory.dart';
import 'package:sariexchange_mobile/models/inventoryItem.dart';
import 'package:sariexchange_mobile/services/inventoryService.dart';
import 'package:sariexchange_mobile/shared/inventoryTiles/inventoryTilesSwiper.dart';

class OurPicks extends StatefulWidget {

  @override
  OurPicksState createState() => OurPicksState();
}

class OurPicksState extends State<OurPicks> with AutomaticKeepAliveClientMixin {
  InventoryService invService;
  Future<Inventory> inventoryFuture;

  OurPicksState() {
    this.invService = InventoryService();
    this.inventoryFuture = this.invService.fetchInventory();
    
  }

  Widget build(BuildContext context) {
    super.build(context);

    return ListView(
      shrinkWrap: true,
      padding: const EdgeInsets.symmetric(vertical: 20.0),
      children: [
        InventoryTilesSwiper(
            title: 'Trending',
            inventoryItemsStream: this.invService.inventorysStream(),
        )
      ]
    );
  }

  @override
  bool get wantKeepAlive => true;  
}


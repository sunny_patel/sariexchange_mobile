import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:uuid/uuid.dart';

class Helpers {

  static uuidV1() {
    var uuid = Uuid();
    return uuid.v1();
  }

  static printPrice(String price) {
    try {
      final formatter = NumberFormat.currency();
      price = price.replaceAll(',', '');
      double number = double.parse(price);
      var formattedPrice = formatter.format(number);
      return formattedPrice;
    } catch(e) {
      print(e);
      return '';
    }
  }

  static getSampleImage({@required double width, @required double height, String text}) {
    var widthHeight = '${width.floor()}x${height.floor()}';
    text = text == null ? 'Sample+Image' : text.replaceAll(' ', '+');
    var imageUrl = 'https://via.placeholder.com/$widthHeight?text=$text';
    print(imageUrl);
    return imageUrl;
  }

  static getThirdOfHeight(BuildContext context) {
    return (MediaQuery.of(context).size.height + 80) / 3;
  }

  static getHeight(BuildContext context) {
    return MediaQuery.of(context).size.height;
  }

  static getWidth(BuildContext context) {
    return MediaQuery.of(context).size.width;
  }
}